import 'dart:async';

class Bloc {
  int _counter = 0;
  StreamController _counterController = StreamController<int>();
  StreamSink get addToStream => _counterController.sink;
  Stream get counterStream => _counterController.stream;

  void increaseCounter() {
    _counter++;
    addToStream.add(_counter);
  }
}
